data {
  // actual observed data
  int<lower = 0> N;                            // number of comparisons in data
  real<lower = -1, upper = 1> obs_speedup[N];  // observed speedup in each comparison

  // parameters to define prior
  int<lower = 1, upper = 3> prior_kind;        // kind of prior:
															  // 1: uniform(-1, 1)
                                               // 2: truncated normal
                                               // 3: shifted normal(prior_pars)
  real<lower = 0> max_speedup;                 // maximum absolute speedup in prior
                                               // (only for normal prior)
  real prior_pars[2];                          // mean, stdev of prior's shifted normal
}

parameters {
  real<lower = -1, upper = 1> speedup;        // actual speedup (hypothesis)
  real<lower = 0> delta_std;                  // stddev of deltas in likelihood
}

model {
  // prior
  if (prior_kind == 1)
	 speedup ~ uniform(-1, 1);
  else if (prior_kind == 2)
	 speedup ~ normal(0, max_speedup) T[-1, 1];
  else if (prior_kind == 3)
	 speedup ~ normal(prior_pars[1], prior_pars[2]) T[-1, 1];
  // likelihood
  for (k in 1:N) {
	 // delta = obs_speedup[k] - speedup ~ normal(0, delta_std)
	 obs_speedup[k] ~ normal(speedup, delta_std);
  }
}
