# Bayesian Data Analysis in Empirical Software Engineering Research #

This repository contains analysis scripts and datasets used in the paper:
**Bayesian Data Analysis in Empirical Software Engineering Research**
available at [arXiv.org 1811.05422](https://arxiv.org/abs/1811.05422).

Each subdirectory `autotest` and `rosetta` contains the material
related to the corresponding reanalysis described in the paper.

If you have any questions about this repository, contact [Carlo A. Furia](https://bugcounting.net).

### Content of `autotest` ###

* `autotest.R`                  analysis script
* `fixing.xslx`                 dataset from paper _[Do Automatically Generated Test Cases Make Debugging Easier? An Experimental Assessment of Debugging Effectiveness and Efficiency](https://doi.org/10.1145/2768829)_
                                  with some amendments following an email exchange with [one of the authors](http://selab.fbk.eu/ceccato/)


### Content of `rosetta` ###

* `rosetta.R`                    main analysis script, also callable from the command line
* `analysis.R`                   functions for the analysis (data processing, statistical analysis, plots, graphs, and tables)
* `rosetta.stan`                 Stan program used by the Bayesian analysis
* `toTikZ.R`                     function to encode graphs in TikZ LaTeX format
* `rosetta_runtime.dsv`        dataset from paper _[A Comparative Study of Programming Languages in Rosetta Code](https://doi.org/10.1109/ICSE.2015.90)_
                                  obtained by merging data from [https://bitbucket.org/nanzs/rosettacodedata/](this public repository)
* `todo.csv`                     information about Rosetta code tasks selected by the analysis of paper _[A Comparative Study of Programming Languages in Rosetta Code](https://doi.org/10.1109/ICSE.2015.90)_
                                  available in [https://bitbucket.org/nanzs/rosettacodedata/](this public repository)
* `u64q_bulkdata.csv`          additional data about programming language performance, obtained from
                                  _[The Computer Language Benchmarks Game](http://benchmarksgame.alioth.debian.org/)_
